import edu.princeton.cs.algs4.BreadthFirstPaths;
import edu.princeton.cs.algs4.CC;
import edu.princeton.cs.algs4.Graph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class GraphProperties {
	private int[] eccentricity;
	
	//Calculate graph properties for the graph G.
	public GraphProperties(Graph g) {
		CC cc = new CC(g);
		if  (cc.count() > 1) {
			throw new IllegalArgumentException("G is not connected");
		}
			eccentricity = new int[g.V()];
	        for (int i = 0; i < g.V(); i ++) {
	        eccentricity[i] = eccentricity(g,i);
	        }
	}
	//Eccentricity of v.
	public int eccentricity(Graph g, int v) {
		BreadthFirstPaths bfp = new BreadthFirstPaths(g, v);
		int eccentricity = 0;
		for (int i = 0; i < g.V(); i++) {
			if(bfp.distTo(i) > eccentricity)
			eccentricity = bfp.distTo(i);
		}
		return eccentricity;		
	}
	//Diameter of G.
	public int diameter() {
	
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < eccentricity.length; i++) {
			if(eccentricity[i] > max) {
				max = eccentricity[i];
			}
		}
		return max;
	}
	//Radius of G.
	public int radius() {
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < eccentricity.length; i++) {
			if(eccentricity[i] < min) {
				min = eccentricity[i];	
			}
		}
		return min;
	}
	//Centers of G.
	public int centers(){
		int centers = 0;
		int radius = radius();
		for(int i = 0; i < eccentricity.length; i++) {
			if(eccentricity[i] == radius) {
				centers = i;
				break;
			}
		}
		return centers;
	}
	//Test client. 
	public static void main(String[] args) {
		In in = new In("C:\\Users\\snico\\nicolich_2018\\tinyG2.txt");
		In in2 = new In("C:\\Users\\snico\\nicolich_2018\\mediumG.txt");
		
		Graph g = new Graph(in);
		GraphProperties gp = new GraphProperties(g);
		Graph g2 = new Graph(in2);
		GraphProperties gp2 = new GraphProperties(g2);
		
		StdOut.println("TinyG2 Graph Properties: ");
		StdOut.println("Diameter = " + gp.diameter());
		StdOut.println("Radius = " + gp.radius());
		StdOut.println("Center = " + gp.centers());
		StdOut.println();
		//StdOut.println("Eccentricity = " + gp.eccentricity());
		
		StdOut.println("MediumG Graph Properties: ");
		StdOut.println("Diameter = " + gp2.diameter());
		StdOut.println("Radius = " + gp2.radius());
		StdOut.println("Center = " + gp2.centers());
	}
}
